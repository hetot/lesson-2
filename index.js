function is_divisible(x) {
    for (let i = 1; i <= x; i++) {
        if (i % 3 == 0 && i % 5 != 0){
            console.log(String(i) + ' is divisible by 3');
        } else if (i % 5 == 0 && i % 3 != 0){
            console.log(String(i) + ' is divisible by 5');
        } else if (i % 5 == 0 && i % 3 == 0){
            console.log(String(i) + ' is divisible by 3 and 5');
        }
    }
}

function check_num(x) {
    if (Number.isInteger(x) && x > 0) {
        if (x % 2 == 0) {
            return 'even';
        } else {
            return 'odd';
        }
    }
}

function sort_array(x) {
    x.sort();
    return x;
}

function unique(x) {
    let my_set = new Set();
    for (let i = 0; i < x.length; i++) {
        for (const [key, a] of Object.entries(x[i])) {
            for (let j = 0; j < a.length; j++) {
                my_set.add(a[j]);
            }
        }
    }
    return my_set;
}